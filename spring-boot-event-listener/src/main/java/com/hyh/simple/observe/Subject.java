package com.hyh.simple.observe;

import com.hyh.simple.observe.Observer;

/**
 * 主题接口
 * @author Summerday
 */
public interface Subject {

    void registerObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyObservers(String message);
}
