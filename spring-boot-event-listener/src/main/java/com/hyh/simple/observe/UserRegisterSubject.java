package com.hyh.simple.observe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Summerday
 */
@Component
public class UserRegisterSubject implements Subject {

    @Autowired
    List<Observer> observers;

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(String username) {
        for (Observer observer : observers) {
            observer.update(username);
        }
    }
}
