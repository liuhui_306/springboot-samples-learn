package com.hyh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync // 开启异步
@SpringBootApplication
public class SpringBootEventListenerApplication {

    public static void main(String[] args) {

        SpringApplication.run(SpringBootEventListenerApplication.class, args);
    }

}
