package com.hyh.validator.web;

import com.hyh.validator.entity.Person;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;

/**
 * @author Summerday
 */

@RestController
@Validated
public class PersonController {

    @GetMapping("/person/{id}")
    public Person getPerson(@PathVariable("id") @Min(1) Long id){
        return new Person();
    }
}
