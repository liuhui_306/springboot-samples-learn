package com.hyh.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hyh.mybatisplus.entity.User;
import com.hyh.mybatisplus.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Summerday
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceTest {

    @Autowired
    UserService userService;

    @Test
    public void getOne() {
        User one = userService.getOne(new QueryWrapper<User>().like("name", "天"), false);
        System.out.println(one);
    }


    @Test
    public void saveBatch() {
        List<User> users = new LinkedList<>();
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setName("saveBatch - " + i);
            user.setAge(i * 10);
            users.add(user);
        }
        boolean b = userService.saveBatch(users);
        System.out.println(b);
    }

    @Test
    public void select() {
        List<User> list = userService.list();
        list.forEach(System.out::println);
    }

    @Test
    public void saveOrUpdateBatch() {
        User user = new User();
        user.setId(1319959148149985284L);
        user.setName("add1");
        user.setAge(30);

        User user2 = new User();
        user.setId(1319959148149985283L);
        user.setName("add2");
        user.setAge(10);

        List<User> list = Arrays.asList(user, user2);
        boolean b = userService.saveOrUpdateBatch(list);
        System.out.println(b);
    }

    /**
     * 链式查询
     */
    @Test
    public void chain() {
        List<User> users = userService
                .lambdaQuery()
                .gt(User::getAge, 25)
                .like(User::getName, "雨")
                .list();
        for (User user : users) {
            System.out.println(user);
        }
    }

    /**
     * 链式更新/删除
     */
    @Test
    public void updateChain() {
        boolean update = userService
                .lambdaUpdate()
                .eq(User::getAge, 25)
                .set(User::getAge, 26)
                //.update();
                .remove();
        System.out.println(update);
    }
}
