package com.hyh.mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hyh.mybatisplus.entity.User;

/**
 * Service接口,继承IService
 * @author Summerday
 */
public interface UserService extends IService<User> {
}
