package com.hyh.shiro.service;

import com.hyh.shiro.database.DataSource;
import com.hyh.shiro.entity.UserBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author Summerday
 */
@Service
public class UserService {

    public UserBean getUser(String username) {
        return DataSource.getData().get(username);
    }

}
