package com.hyh.shiro.database;

import com.hyh.shiro.entity.UserBean;
import org.apache.shiro.crypto.hash.Hash;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 准备实验数据
 * @author Summerday
 */
public class DataSource {

    /**
     * k 用户名
     * v 用户信息
     */
    private static final Map<String, UserBean> data = new HashMap<>();

    static {
        data.put("hyh",new UserBean("hyh","123456","user","view",false));
        data.put("lock_user",new UserBean("lock_user","123456","user","view",true));
        data.put("sum",new UserBean("sum","123456","admin","view,edit",false));
    }

    public static Map<String, UserBean> getData() {
        return data;
    }
}
