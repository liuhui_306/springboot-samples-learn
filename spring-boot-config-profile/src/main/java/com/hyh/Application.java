package com.hyh;


import com.hyh.config.AcmeProperties;
import com.hyh.config.AuthorProperties;
import com.hyh.config.HyhConfigurationProperties;
import com.hyh.config.MyProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

/**
 * @author Summerday
 */

@Configuration
public class Application implements CommandLineRunner {

    @Value("${name}")
    private String name;

    @Autowired
    private AuthorProperties authorProperties;

    @Autowired
    private HyhConfigurationProperties hyhConfigurationProperties;

    @Autowired
    private MyProperties myProperties;

    @Autowired
    private AcmeProperties acmeProperties;


    @Override
    public void run(String... args) throws Exception {
        System.out.println(authorProperties);
        System.out.println("------------------------------");
        System.out.println(hyhConfigurationProperties);
        System.out.println("------------------------------");

        System.out.println(myProperties);
        System.out.println("------------------------------");
        System.out.println(name);
        System.out.println("------------------------------");
        System.out.println(acmeProperties);
    }
}
