package com.hyh.config;

import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author Summerday
 */
@Configuration
public class AuthorProperties {

    @Value("${author.name}")
    private String name;

    @Value("${author.age}")
    private int age;

    @Value("${author.dogs}")
    private int[] dogs;

    @Value("#{'${author.hobbies}'.split(',')}")
    private List<String> hobbies;

    @Value("#{${author.contact}}")
    private Map<String, String> contact;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"name\":\"")
                .append(name).append('\"');
        sb.append(",\"age\":")
                .append(age);
        sb.append(",\"dogs\":")
                .append(Arrays.toString(dogs));
        sb.append(",\"hobbies\":")
                .append(hobbies);
        sb.append(",\"contact\":")
                .append(contact);
        sb.append('}');
        return sb.toString();
    }
}
