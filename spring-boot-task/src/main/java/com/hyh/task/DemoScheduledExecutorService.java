package com.hyh.task;

import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Summerday
 */
public class DemoScheduledExecutorService {

    //延时时间
    private static final long DELAY = 3000;

    //间隔时间
    private static final long PERIOD = 5000;

    public static void main(String[] args) {

        Runnable task = new Runnable() {
            @Override
            public void run() {
                System.out.println("任务执行 --> " + LocalDateTime.now());
            }
        };

        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(task, DELAY, PERIOD, TimeUnit.MILLISECONDS);

    }
}
