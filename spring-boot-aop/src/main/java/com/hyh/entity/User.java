package com.hyh.entity;

import lombok.Data;
import lombok.ToString;

/**
 * @author Summerday
 */

@Data
@ToString
public class User {

    private Long id;
    private String username;
    private Integer age;
}
