package com.hyh.thymeleaf.controller;

import com.hyh.thymeleaf.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Summerday
 */

@Controller
public class ExampleController {

    @GetMapping("/if")
    public String ifunless(Model model){
        model.addAttribute("flag",true);
        return "if";
    }

    @GetMapping("/eq")
    public String eq(Model model){
        model.addAttribute("user",new User("5","summer",40));
        return "eq";
    }

    @GetMapping("/switch")
    public String sw(Model model){
        model.addAttribute("sex","man");
        return "switch";
    }

    @GetMapping("/string")
    public String str(Model model){
        model.addAttribute("id",5);
        model.addAttribute("username","天乔巴夏");
        return "string";
    }

}
